fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((data) => showPosts(data));

const showPosts = (posts) => {
  let postEntries = "";
  let post = {};
  for (i = 0; i < posts.length; i++) {
    post = posts[i];
    postEntries += `<div id="posts-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button onclick=editPost(${post.id}) id="btn-edit-${post.id}">Edit</button>
      <button onclick=deletePost(${post.id}) id="btn-delete-${post.id}">Delete</button>
    </div>`;
  }
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  e.preventDefault();
  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: document.querySelector("#txt-title").value,
      body: document.querySelector("#txt-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully Added!");
      //showPosts(data);
      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-body").value = null;
    });
});

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();
  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    body: JSON.stringify({
      id: document.querySelector("#txt-edit-id").value,
      title: document.querySelector("#txt-edit-title").value,
      body: document.querySelector("#txt-edit-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully updated!");
      document.querySelector("#txt-edit-id").value = null;
      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-body").value = null;
      document.querySelector("#btn-submit-update").disabled = true;
    });
});

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  document.querySelector("#btn-submit-update").disabled = false;
};

const deletePost = (id) => {
  document.querySelector(`#posts-${id}`).remove();
  // let formElement = document.querySelector(`#post-title-${id}`);
  // formElement.parentElement.replaceChildren();
};
